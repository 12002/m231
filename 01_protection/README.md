# Schutzwürdigkeit von Daten im Internet <!-- omit in toc -->

Im [Kompetenzband](../00_evaluation/) finden Sie eine Übersicht über Handlungskompetenzen, die Sie im Modul 231 zu erwerben haben. Welche Kompetenzen jeweils in einem Themenblock behandelt werden, finden Sie jeweils im Abschnitt *Ziele*.


# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Ziele](#1-ziele)
- [2. Leseauftrag - EDÖB - Datenschutz Informationsdossier](#2-leseauftrag---edöb---datenschutz-informationsdossier)
  - [2.1. Unterlagen EDÖB](#21-unterlagen-edöb)
- [3. Wichtigste Begriffe klären](#3-wichtigste-begriffe-klären)
  - [3.1. Aufgabenstellung](#31-aufgabenstellung)
    - [3.1.1. Variante 1](#311-variante-1)
    - [3.1.2. Variante 2](#312-variante-2)
- [4. Video zum Thema Datenschutz (SRF)](#4-video-zum-thema-datenschutz-srf)
  - [4.1. Aufgabenstellung](#41-aufgabenstellung)
- [5. Nutzungsbestimmungen bekannter Plattformen prüfen](#5-nutzungsbestimmungen-bekannter-plattformen-prüfen)
  - [5.1. Aufgabenstellung](#51-aufgabenstellung)
- [6. Einführung in das Thema Datenschutz](#6-einführung-in-das-thema-datenschutz)
- [7. Checklisten des Datenschutzbeauftragten des Kantons Zürichs](#7-checklisten-des-datenschutzbeauftragten-des-kantons-zürichs)
  - [7.1. Aufgabenstellung](#71-aufgabenstellung)
  - [7.2. Markdownformat-Beispiel](#72-markdownformat-beispiel)
- [8. Auskunft über eigene Personendaten verlangen](#8-auskunft-über-eigene-personendaten-verlangen)
  - [8.1. Aufgabenstellung](#81-aufgabenstellung)
- [9. Mein soziales Netzwerk](#9-mein-soziales-netzwerk)
  - [9.1. Übersicht](#91-übersicht)
  - [9.2. Vier Phasen](#92-vier-phasen)
  - [9.3. Phase 1 - Kreativer Austausch (20 min)](#93-phase-1---kreativer-austausch-20-min)
  - [9.4. Phase 2 - Vorbereitung der Präsentation (20 min)](#94-phase-2---vorbereitung-der-präsentation-20-min)
  - [9.5. Phase 3 - Präsentationsphase](#95-phase-3---präsentationsphase)
  - [9.6. Phase 4 - Abstimmung](#96-phase-4---abstimmung)
- [10. Quellen](#10-quellen)

# 1. Ziele
 - A1G: Kann schützenswerte Daten erkennen.
 - A1F: Wendet in seiner Umgebung den Schutz der Daten konsequent an und befolgt Regeln.
 - A1E: Kann unterschiedliche Regelungen (zBps DSG, DSGVO, Schulordnung) vergleichen und kritisch hinterfragen.
 - F1G: Kann die Problematik bei unsicher konfigurierten Anwendungen erläutern.
 - F2G: Kennt die Bedeutung der AGBs und Datenschutzrichtlinien von Anbietern.
 - F2F: Kann die Datenschutzrichtlinie bezüglich Datenspeicherung überprüfen.

---

# 2. Leseauftrag - EDÖB - Datenschutz Informationsdossier
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  bis LB1 |
| Ziel | Wichtigste Grundlagen zum Thema Datenschutz lernen |

Der Eidgenössischer Datenschutz und Öffentlichkeitsbeauftragter (EDÖB) stellt Lehrmittel zum Thema Datenschutz zur Verfügung. Die Lehrmittel geben einen umfassenden Einblick in die Bedeutung des Datenschutzes im Umgang mit den neuen Medien. Es hat zum Ziel Lernende für eine verantwortungsvolle Verwendung von Personendaten zum Schutz ihrer Privatsphäre und die Rücksicht auf die Persönlichkeit zu sensibilisieren. 

Das Informationsdossier ist zu Beginn des Moduls 231 in drei Abschnitten als Hausaufgabe zu lesen. Die Daten werden von Lehrperson separat bekannt gegeben. Der Inhalt des Dossiers ist Teil der LB1.

 - "Was ist Datenschutz?" (bis und mit Seite 9)
 - "Moderne Informationstechnologie und ihre Risiken von Internet bis Videotelefonie", bis und mit Seite 23
 - "Moderne Informationstechnologie und ihre Risiken - ab Bilder und Bildrechte", bis und mit Seite 33

**Tipp:** Sie müssen für die LB1 eine Zusammenfassung mitbringen. Fassen Sie das gelesene laufend zusammen. Das unterstützt das Verständnis und dient automatisch als Prüfungsvorbereitung. 

## 2.1. Unterlagen EDÖB
Alle Unterlagen erhalten Sie einerseits von der Lehrperson und können andererseits direkt auf der Seite des EDÖB heruntergeladen werden. 

[Link zu den Lehrmittel EDÖB 16 - 19 jährige](https://www.edoeb.admin.ch/edoeb/de/home/datenschutz/Internet_und_Computer/jugend-und-internet/lehrmittel-datenschutz/lehrmittel--elementare-datensicherheit--fuer-16-19-jaehrige.html)

---

# 3. Wichtigste Begriffe klären
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit (3 bis 4 Personen) |
| Aufgabenstellung  | Erfahrungsaustausch |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Wichtigste Fragen gemeinsam Diskutieren, Wichtigste Punkte auf Poster festhalten |

## 3.1. Aufgabenstellung
Setzen Sie sich in der Gruppe zusammen, diskutieren und recherchieren Sie gemeinsam zum Thema Datenschutzgesetz. Notieren Sie die wichtigsten Erkenntnisse auf einem Poster (Sie dürfen ihre Erkenntnisse auch grafisch festhalten). Anschliessend präsentiert jemand der Gruppe das Poster. Nutzen Sie als Grundlage / Inspiration die nachfolgenden Fragen: 

Jede Gruppe diskutiert folgende Fragen und hält die wichtigsten Punkte stichwortartig auf dem Poster fest. 
 - Ist Datenschutz wichtig?
 - Weshalb ist Datenschutz für mich wichtig?
 - Vor was schützt Sie das Datenschutzgesetz?
 
Jede Gruppe erhält von der Lehrperson eine Nummer zugeteilt. Beantworten Sie nur die Frage mit Ihrer Nummer.

### 3.1.1. Variante 1
 1. Das eigene Bild: Alles was recht ist. - Die wichtigsten Punkte
 2. Pornografie: Alles was recht ist. - Die wichtigsten Punkte
 3. Cybermobbing: Alles was Recht ist. - Die wichtigsten Punkte
 4. Datenschutz - Schutz der Personendaten - Was sagt die Menschenrechtskonvention dazu?
 5. Rechtfertigungsgrund - Was ist das? In welchen Themen brauchen wir Rechtfertigungsgründe? Darf ein ein Online-Shop nach der Blutgruppe fragen? Weshalb nicht?


### 3.1.2. Variante 2
 
 1. Was ist ein Meme? Welche Memes sind erlaubt, welche nicht? Gibt es Regeln?
 2. Wo finde ich das Datenschutzgesetz? Von wann ist die aktuelle Fassung? Was steht da so drin?
 3. Welche Länder haben einen gleichwertiges Datenschutzgesetz? Was sind meine Rechte (Stichwort Auskunftsrecht)?
 4. Was ist Cybermobbing? Hat Cybermobbing etwas mit Datenschutz zu tun? Begründen Sie. 
 5. Was ist sicherer: Telegram, Signal, Whats-App? Weshalb?
 6. Was ist Identitätsdiebstahl? Was sind die Folgen?

---

# 4. Video zum Thema Datenschutz (SRF)
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Video anschauen und kritisch hinterfragen |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie setzen sich kritisch mit einem Video zum Thema Datenschutz auseinander und analysieren welche Dinge immer noch gelten und welche sich verändert haben. |

## 4.1. Aufgabenstellung
Schauen Sie sich den [14-minütigen Film zum Thema Datenschutz](https://www.srf.ch/sendungen/myschool/datenschutz-2) aus dem Jahre 2013 an. 

Diskutieren Sie mit Ihren Mitlernenden folgende Fragen:
 - Was trifft heute immer noch zu?
 - Was trifft heute nicht mehr zu (Was hat sich verändert)?

---
# 5. Nutzungsbestimmungen bekannter Plattformen prüfen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit, Gruppenarbeit |
| Aufgabenstellung  | Recherche |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Nutzungsbestimmungen von Sozialen Netzwerken finden. Jeder lernende weiss, welche die wichtigsten und am häufigsten beschrieben Punkte in solchen Nutzungsbestimmungen sind.  |

## 5.1. Aufgabenstellung
 - Pro Gruppe sollen 3 bis 4 Personen eine Plattform recherchieren. Beispiel für Plattformen: Reddit, Twitter, Instagram, TikTik, Discord, Signal, Whats-App, ...)
 - Die Plattformen werden unter den Lernenden aufgeteilt.
 - Zu jeder Plattform sollen folgende Fragen beantwortet werden:
   - Wo finde ich die Datenschutz-/Nutzungsbestimmungen (URL)?
   - Welche Daten (z.B. Personendaten) werden gespeichert?
   - Für was werden die Daten verwendet?
   - Werden die Daten weitergegeben? Wenn ja, an wen?
   - Darf der Dienst/Service/Plattform meine Bilder/Videos weiterverkaufen?
 - Die Ergebnisse werden anschliessend auf einem gemeinsamen Powerpoint festgehalten. Pro Plattform 1 bis max. 2 Folien. 


# 6. Einführung in das Thema Datenschutz
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Plenum |
| Aufgabenstellung  | Präsentation und Erklärungen der Lehrperson verfolgen und bei bedarf Notizen machen. |
| Zeitbudget  |  1 Lektion |
| Ziel | Jeder Lernende kennt nicht rechtliche Grundlagen zum Datenschutz und weiss was Sorgfaltspflicht für Ihn als Informatiker bedeutet   |

---

# 7. Checklisten des Datenschutzbeauftragten des Kantons Zürichs
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Prüfen der eigenen Infrastruktur mithilfe von Checklisten |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Jeder Lernende hat einen Teil seiner persönlichen Infrastruktur geprüft, protokolliert und die notwendigen Verbesserungsmassnahmen festgelegt und wenn möglich umgesetzt.  |

## 7.1. Aufgabenstellung
 - Besuchen Sie die Webseite https://www.datenschutz.ch/meine-daten-schuetzen
 - Wählen zwei Checklisten aus und gehen Sie diese Schrittweise durch. 
 - Notieren Sie zu jedem Punkt, ob Sie
   - das bereits erfüllt haben.
   - Wenn ja: Wie haben diesen Punkt erfüllt?
   - Wenn nein: Was können Sie machen um diesen Punk zu erfüllen?
 - Wichtig: Sie müssen auch wissen, wie sie es umsetzen! (Siehe Beispiel unten)
 - Halten Sie Ihre Ergebnisse im Markdown-Format in einem Textdokument fest. Das wird später Teil Ihres Portfolios werden. 

## 7.2. Markdownformat-Beispiel 
Ihre Dokumentation sollte ungefähr so aussehen:
```
# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

# Checkliste Smartphone-Sicherheit erhöhen
 - Link: https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen
 - 01 Gerätesperren aktivieren
    - Ja, ich habe eine Gerätesperre mit PIN aktiviert. 
 - 02 Gerät bei Verlust sofort sperren und löschen lassen.
    - Kannte ich noch nicht, habe https://www.google.com/android/find besucht und mir die funktionen angeschaut. Jetzt weiss ich wie ich im Ernstfall handeln muss. 
 - 03 Apps aus vertrauenswürdigen Quellen installieren
    - Ich installiere alle Apps aus dem Google Playstore und prüfe bei jeder App, welche Zugriffsrechte Sie hat. Ich bin alle Berechtigungseinstellungen für jedes App im meinem Telefon durchgegangen und wenn nötig Änderungen vorgenommen
 - 04 ...
```

---

# 8. Auskunft über eigene Personendaten verlangen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Öffentliche Webseiten zum Thema Auskunftsrecht besuchen und Musterbrief studieren |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Jeder Lernende weiss wo er Informationen zu seinem Auskunftsrecht findet und kennt das Verfahren  |

## 8.1. Aufgabenstellung
 - Besuchen Sie die beiden Webseiten https://www.datenschutz.ch/meine-rechte-einfordern und https://www.edoeb.admin.ch/edoeb/de/home/dokumentation/datenschutz/musterbriefe/wie-sie-vorgehen--wenn-sie-ein-auskunftsbegehren-stellen-moechte.html
 - Angenommen Sie wollen wissen, welche Personendaten Ihr Mobilfunkanbieter über Sie gespeichert hat. **Wie müssen Sie vorgehen?**
 - Laden Sie sich den Musterbrief herunter und füllen Sie diesen Proforma aus. 


# 9. Mein soziales Netzwerk
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Sie machen sich grundüberlegungen zu Ihrem eigenen Sozialen Netzwerk |
| Zeitbudget  |  1 Lektionen |
| Ziel | Perspektivenwechsel: Datenschutzaspekte aus der Sicht des Betreibers eines sozialen Netzwerkes.  |

## 9.1. Übersicht

Die Klasse wird in Gruppen à 3 bis 4 Personen aufgeteilt. Jede Gruppe kreiert ein soziales Netzwerk und "verkauft" (präsentiert) es der Klasse. Jede(r) Lernende stimmt für ein soziales Netzwerk ab (ausser das eigene).

## 9.2. Vier Phasen
 1. Kreativer Austausch - Sie kreieren ihr Soziales Netzwerk
 2. Sie bereiten die Präsentation Ihres Sozialen Netzwerkes vor. 
 3. Präsentation - Sie haben 3 Minuten um ihr Soziales Netzwerk der Klasse zu präsentieren. 
 4. Abstimmung - Die Klasse stimmt ab, welches Soziale Netzwerk gewinnt. 

## 9.3. Phase 1 - Kreativer Austausch (20 min)
Diskutieren Sie mit Ihren Kollegen und beantworten Sie diese Fragen für Ihr soziales Netzwerk. Halten Sie Ihre Antworten in Ihrem Portfolio fest. 
 1. Wie heisst das neue soziale Netzwerk? Was ist das Motto/Slogan?
 2. Was ist der Sinn und Zweck des Netzwerkes?
 3. Welche Posts/Einträge/Inhalte können hochgeladen und veröffentlicht werden?
 4. Was kann nicht veröffentlicht werden? Warum nicht?
 5. Welche Informationen kann/muss ein Nutzer von sich preisgeben, um sich registrieren zu können?
 6. Wie bewegen Sie potenzielle Nutzer dazu, sich ihrem Netzwerk anzuschliessen?
 7. Was ist der Unterschied zu den bereits bestehenden Netzwerken (z.B. Instagram, Facebook, Twitter, Pinterest, Xing, etc.)?
 8. Gibt es weitere Einschränkungen, was man auf dem Netzwerk darf und was nicht?
 9. Wie finanzieren Sie Ihr Soziales Netzwerk (Serverkosten, Löhne, usw.)?

## 9.4. Phase 2 - Vorbereitung der Präsentation (20 min)
Ziel ist es in 3 Minuten möglichst viele von Ihren Mitschülern von Ihrem Sozialen Netzwerk zu überzeugen. In welcher Form ihrer Präsentation steht Ihnen frei: Powerpoint, eine Animation, ein Poster, ein kurzer Sketch, ein kurzes Filmchen, eine Radiowerbung. Seien Sie kreativ!

## 9.5. Phase 3 - Präsentationsphase
Jede Gruppe führt Ihre Präsentation durch. Denken Sie daran: Wer sein Netzwerk am besten verkauft, gewinnt!

Nach ihrer 3 Minütigen Präsentation stehen der Klasse und die Lehrperson 5 Minuten für Fragen zur Verfügung.

## 9.6. Phase 4 - Abstimmung
Welches Konzept hat Sie überzeugt? Bei welchem Sozialen Netzwerk würden Sie gerne mitmachen?

Stimmen Sie ab. Natürlich NICHT für Ihr Eigenes!

---

# 10. Quellen
 - https://www.srf.ch/sendungen/myschool/datenschutz-2
 - https://www.digitale-gesellschaft.ch/auskunftsbegehren/
 - https://www.fedlex.admin.ch/eli/cc/1993/1945_1945_1945/de
 - https://www.economiesuisse.ch/de/artikel/datenschutz-eine-uebersicht-zum-neuen-gesetz
 - https://www.bj.admin.ch/dam/bj/de/data/staat/gesetzgebung/datenschutzstaerkung/vorentw-d.pdf.download.pdf/vorentw-d.pdf
 - https://www.bj.admin.ch/bj/de/home/staat/gesetzgebung/datenschutzstaerkung.html
 - https://www.kmu.admin.ch/kmu/de/home/aktuell/news/2021/neues-datenschutzgesetz-kmu-muessen-sich-anpassen.html
 - https://www.economiesuisse.ch/de/artikel/datenschutz-eine-uebersicht-zum-neuen-gesetz
 - https://www.economiesuisse.ch/de/artikel/datenschutzgesetz-bereiten-sie-sich-jetzt-vor
 - https://www.economiesuisse.ch/de/datenwirtschaft
 - https://www.rosenthal.ch/downloads/Rosenthal-Webinar-DSG-GDPR.pdf
 - https://www.myright.ch/de/business/rechtstipps/datenschutz/dsg-neu
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz.html
 - https://www.stadt-zuerich.ch/pd/de/index/stadtpolizei_zuerich/praevention/digitale-medien/Gefahren/falsche-identitaeten.html
 - https://www.verbraucherzentrale.de/wissen/digitale-welt/datenschutz/welche-folgen-identitaetsdiebstahl-im-internet-haben-kann-17750
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - [https://de.wikipedia.org/wiki/Richtlinie_(EU)_2019/790_(Urheberrecht_im_digitalen_Binnenmarkt)](https://de.wikipedia.org/wiki/Richtlinie_(EU)_2019/790_(Urheberrecht_im_digitalen_Binnenmarkt))
 - https://www.20min.ch/story/das-bedeutet-das-neue-eu-gesetz-fuer-deine-memes-755504826807
 - https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A02019L0790-20190517&qid=1629634471866
 - https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/verbraucherinnen-und-verbraucher_node.html
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz/ueberblick/datenschutz.html