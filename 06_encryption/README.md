# Verschlüsselung

# Caesar
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Klassenchallenge |
| Aufgabenstellung  | Nachrichten verschlüsseln und Entschlüsseln, CrypTool kennenlernen |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie lernen das CrypTool kennen. |

## Aufgabenstellung

In ersten Schritt verschlüsseln Sie mithilfe der [Caesar-Verschlüsselung](https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung) eine Nachricht und schicken diese dem Kollegen der nach Ihnen in der Klassenliste folgt. Im zweiten Schritt versuchen Sie die Nachricht, die Sie vom Klassenkameraden vor Ihnen in der Klassenliste steht, zu entschlüsseln. Dafür verwenden Sie zwei verschiedene Tools: 
 - [CrypTool-Online - Caesar](https://www.cryptool.org/en/cto/caesar)
 - [CrypTool 2](https://www.cryptool.org/en/ct2/)



### Teil 1 - Verschlüsseln
 - Besuchen Sie die Webseite [CrypTool-Online - Caesar](https://www.cryptool.org/en/cto/caesar). Fügen Sie Ihre Nachricht im Feld *Input (plaintext)* ein und wählen einen zufälligen Key aus. Wenn Ihnen keine Nachricht einfällt, nehmen sie doch ein  [zufälliges Zitat](http://zitate.net/zuf%C3%A4llige-zitate).
 - Wählen Sie nun eine zufällige Zahl (Ihren geheimen Schlüssel) aus und stellen diesen bei Key ein. 
 - Kopieren Sie Wert im Feld *Output (ciphertext)* (Die verschlüsselte Nachricht) und schicken es dem Klassenkamaraden der nach Ihnen in der Klassenliste steht. 

![CrypTool-Online - Caesar - Encryption](./videos/cryptoolonlinecaesar.webm)



### Teil 2 - Entschlüsseln
Nun kommt der spannende Teil. Obwohl es bei der Caesar-Verschlüsselung bei der Verwendung des einfachen Alphabetes nur 26 Buchstaben und demnach nur 25 mögliche Schlüssel gibt und das Durchprobieren dieser Möglichkeiten nicht sehr viel Zeit in Anspruch nimmt. Kann mithilfe der Analyse der Buchstabenhäufigkeit die Caesar-Verschlüsselung einfach geknackt werden. Das Video unten zeigt wie mithilfe des [CrypTool 2](https://www.cryptool.org/en/ct2/downloads) die verschlüsselte Nachricht innerhalb von zwei Minuten "geknackt" bzw. entschlüsselt werden kann. Der verwendete Schlüssel muss dabei nicht bekannt sein. 

**Können Sie die Nachricht Ihres Kollegen mit dieser Methode entschlüsseln?**

![CrypTool2 - Ceasar-Verschlüsselung Knacken](./videos/cryptool2caesardecryption.webm)



### Ausprobieren und ein wenig herumspielen
Mithilfe von CrypTool können Sie unzählig verschiedene klassische und moderne Verschlüsselungsmethoden ausprobieren. Probieren Sie zum Beispiel die [Vigenère-Verschlüsselung](https://de.wikipedia.org/wiki/Vigen%C3%A8re-Chiffre) aus. 

Im nachfolgenden Video sehen Sie, wie Sie mithilfe des Wizards die verschiedenen Verschlüsselungen und Analysemethoden ausprobieren können. 

![CrypTool2 Wizard Vigenère-Verschlüsselung](./videos/cryptool2vigenere.webm)

